package com.example.demo.room.infrastructure;

import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Service
public class RoomIdentifierFactory {
	public String nextID() {
		return UUID.randomUUID().toString();
	}
}
