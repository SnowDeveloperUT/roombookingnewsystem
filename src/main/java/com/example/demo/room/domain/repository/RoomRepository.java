package com.example.demo.room.domain.repository;

import com.example.demo.room.domain.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, String>, CustomRoomRepository {}
