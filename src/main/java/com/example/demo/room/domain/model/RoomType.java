package com.example.demo.room.domain.model;

/**
 * Created by snowwhite on 6/21/2017.
 */
public enum RoomType {
	SINGLE, DOUBLE, TWIN, STUDIO
}
