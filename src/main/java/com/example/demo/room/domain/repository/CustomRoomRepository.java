package com.example.demo.room.domain.repository;

import com.example.demo.room.domain.model.Room;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/21/2017.
 */
public interface CustomRoomRepository {
	List<Room> findAvailable(LocalDate startDate, LocalDate endDate);
	boolean isRoomAvailable(String id, LocalDate startDate, LocalDate endDate);
}
