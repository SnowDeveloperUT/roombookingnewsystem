package com.example.demo.room.rest;

import com.example.demo.room.application.dto.RoomDTO;
import com.example.demo.room.application.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/21/2017.
 */
@RestController
@RequestMapping("/api/rooms")
public class RoomController {

	@Autowired
	RoomService roomService;

	@GetMapping()
	public List<RoomDTO> findAllProperties() {
		return roomService.findAllRooms();
	}

	@GetMapping("/{id}")
	public RoomDTO findRoomById(@PathVariable("id") String id) {
		return roomService.findRoomById(id);
	}


	@GetMapping("/available")
	public List<RoomDTO> findAvailableRooms(
			@RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
			@RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
	) {
		return roomService.findAvailableRooms(startDate, endDate);
	}


}
