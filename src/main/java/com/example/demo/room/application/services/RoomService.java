package com.example.demo.room.application.services;

import com.example.demo.room.application.dto.RoomDTO;
import com.example.demo.room.domain.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Service
public class RoomService {

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	RoomAssembler roomAssembler;

	public List<RoomDTO> findAllRooms() {

		return roomAssembler.toResources(roomRepository.findAll());
	}


	public RoomDTO findRoomById(String id) {
		return roomAssembler.toResource(roomRepository.findOne(id));
	}

	public List<RoomDTO> findAvailableRooms(LocalDate startDate, LocalDate endDate) {

		return roomAssembler.toResources(roomRepository.findAvailable(startDate, endDate));
	}

}
