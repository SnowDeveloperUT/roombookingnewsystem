package com.example.demo.room.application.dto;

import com.example.demo.room.domain.model.Room;
import com.example.demo.room.domain.model.RoomType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class RoomDTO extends ResourceSupport{
	String _id;
	BigDecimal pricePerNight;
	RoomType roomType;


	public Room asRoom(){
		return Room.of(_id, pricePerNight, roomType);
	}
}
