package com.example.demo.room.application.services;

import com.example.demo.room.application.dto.RoomDTO;
import com.example.demo.room.domain.model.Room;
import com.example.demo.room.rest.RoomController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Service
public class RoomAssembler extends ResourceAssemblerSupport<Room, RoomDTO>{

	public RoomAssembler() {
		super(RoomController.class, RoomDTO.class);
	}


	@Override
	public RoomDTO toResource(Room room) {
		RoomDTO dto = createResourceWithId(room.getId(), room);

		dto.set_id(room.getId());
		dto.setPricePerNight(room.getPricePerNight());
		dto.setRoomType(room.getRoomType());


		return dto;
	}
}
