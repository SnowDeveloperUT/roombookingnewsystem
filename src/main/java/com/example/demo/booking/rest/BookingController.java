package com.example.demo.booking.rest;

import com.example.demo.booking.application.dto.BookingDTO;
import com.example.demo.booking.application.services.BookingService;
import com.example.demo.common.application.exceptions.CustomerNotFoundException;
import com.example.demo.common.application.exceptions.RoomNotAvailableException;
import com.example.demo.common.application.exceptions.RoomNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by snowwhite on 6/21/2017.
 */
@RestController
@RequestMapping("/api/bookings")
public class BookingController {

	@Autowired
	BookingService bookingService;

	@PostMapping()
	public ResponseEntity<BookingDTO> createBooking(
			@RequestBody String partialBookingDTO) throws URISyntaxException, CustomerNotFoundException, RoomNotFoundException, RoomNotAvailableException {
		BookingDTO bookingDTO = bookingService.createBooking(partialBookingDTO);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(new URI(bookingDTO.getId().getHref()));

		return new ResponseEntity<>(bookingDTO, headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}/cancel", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public String cancelBooking(@PathVariable("id") String id){
		return "{\"response\": \"" + bookingService.cancelBooking(id) + "\"}";
	}

	@DeleteMapping("/{id}")
	public BookingDTO closeBookingOrder(@PathVariable String id) throws Exception {
		return bookingService.closeBookingOrder(id);
	}

}
