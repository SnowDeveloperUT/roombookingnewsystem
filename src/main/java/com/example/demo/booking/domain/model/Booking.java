package com.example.demo.booking.domain.model;

import com.example.demo.common.domain.model.BusinessPeriod;
import com.example.demo.customer.domain.model.Customer;
import com.example.demo.room.domain.model.Room;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Booking {

	@Id
	String id;

	@ManyToOne
	Customer customer;

	@ManyToOne
	Room room;

	@Embedded
	BusinessPeriod bookingDates;

	@Enumerated(EnumType.STRING)
	BookingStatus bookingStatus;

	public void setBookingStatus(BookingStatus bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

}
