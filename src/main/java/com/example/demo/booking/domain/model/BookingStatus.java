package com.example.demo.booking.domain.model;

/**
 * Created by snowwhite on 6/21/2017.
 */
public enum BookingStatus {
	CREATED, PENDING, OPEN, REJECTED, CLOSED, CANCELLED
}
