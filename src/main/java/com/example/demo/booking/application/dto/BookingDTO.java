package com.example.demo.booking.application.dto;

import com.example.demo.booking.domain.model.BookingStatus;
import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.customer.application.dto.CustomerDTO;
import com.example.demo.room.application.dto.RoomDTO;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Data
public class BookingDTO extends ResourceSupport{
	String _id;

	CustomerDTO customerDTO;

	RoomDTO roomDTO;

	BusinessPeriodDTO bookingDates;

	BookingStatus bookingStatus;
}
