package com.example.demo.booking.application.services;

import com.example.demo.booking.application.dto.BookingDTO;
import com.example.demo.booking.domain.model.Booking;
import com.example.demo.booking.rest.BookingController;
import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.rest.ExtendedLink;
import com.example.demo.customer.application.dto.CustomerDTO;
import com.example.demo.room.application.services.RoomAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.POST;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Service
public class BookingAssembler extends ResourceAssemblerSupport<Booking,BookingDTO>{

	@Autowired
	RoomAssembler roomAssembler;

	public BookingAssembler() {
		super(BookingController.class, BookingDTO.class);
	}

	@Override
	public BookingDTO toResource(Booking booking) {
		BookingDTO dto = createResourceWithId(booking.getId(), booking);

		dto.set_id(booking.getId());
		dto.setCustomerDTO(CustomerDTO.of(booking.getCustomer().getId(),booking.getCustomer().getName(),
				booking.getCustomer().getAddress(),booking.getCustomer().getEmail(),booking.getCustomer().getCustomerType()));
		dto.setRoomDTO(roomAssembler.toResource(booking.getRoom()));
		dto.setBookingDates(BusinessPeriodDTO.of(booking.getBookingDates().getStartDate(),booking.getBookingDates().getEndDate()));
		dto.setBookingStatus(booking.getBookingStatus());

		try {
			switch (dto.getBookingStatus()) {
				case PENDING:
					dto.add(new ExtendedLink(
							linkTo(methodOn(BookingController.class)
									.createBooking(dto.get_id())).toString(),
							"accept", POST));
					dto.add(new ExtendedLink(
							linkTo(methodOn(BookingController.class)
									.cancelBooking(dto.get_id())).toString(),
							"cancel", DELETE));
					break;
				case OPEN:
					dto.add(new ExtendedLink(
							linkTo(methodOn(BookingController.class)
									.closeBookingOrder(dto.get_id())).toString(),
							"close", DELETE));
				default:
					break;
			}
		} catch (Exception e) {
		}

		return dto;
	}
}
