package com.example.demo.booking.application.services;

import com.example.demo.booking.application.dto.BookingDTO;
import com.example.demo.booking.domain.model.Booking;
import com.example.demo.booking.domain.model.BookingStatus;
import com.example.demo.booking.domain.repository.BookingRepository;
import com.example.demo.booking.infrastructure.BookingIdentifierFactory;
import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.application.exceptions.CustomerNotFoundException;
import com.example.demo.common.application.exceptions.RoomNotAvailableException;
import com.example.demo.common.application.exceptions.RoomNotFoundException;
import com.example.demo.customer.domain.model.Customer;
import com.example.demo.customer.domain.repository.CustomerRepository;
import com.example.demo.room.application.services.RoomAssembler;
import com.example.demo.room.domain.model.Room;
import com.example.demo.room.domain.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Service
public class BookingService {

	@Autowired
	RoomRepository roomRepository;

	@Autowired
	RoomAssembler roomAssembler;

	@Autowired
	BookingRepository bookingRepository;

	@Autowired
	BookingAssembler bookingAssembler;

	@Autowired
	CustomerRepository customerRepository;

	BookingIdentifierFactory bookingIdentifierFactory = new BookingIdentifierFactory();

	public BookingDTO createBooking(BookingDTO partialBookingDTO) throws RoomNotFoundException, CustomerNotFoundException, RoomNotAvailableException {

		Customer customer = customerRepository.findOne(partialBookingDTO.getCustomerDTO().get_id());
		Room room = roomRepository.findOne(partialBookingDTO.getRoomDTO().get_id());

		if(customer == null){
			throw new CustomerNotFoundException(partialBookingDTO.getCustomerDTO().get_id());
		}

		if(room == null){
			throw new RoomNotFoundException(partialBookingDTO.getRoomDTO().get_id());
		}

		BusinessPeriodDTO period = partialBookingDTO.getBookingDates();
		long numDays = ChronoUnit.DAYS.between(period.getStartDate(), period.getEndDate());
		BigDecimal totalPrice = room.getPricePerNight().multiply(BigDecimal.valueOf(numDays + 1));

		boolean isRoomAvailable = roomRepository.isRoomAvailable(partialBookingDTO.getRoomDTO().get_id(),
				partialBookingDTO.getBookingDates().getStartDate(),
				partialBookingDTO.getBookingDates().getEndDate());

		if(isRoomAvailable){

			Booking booking = Booking.of(bookingIdentifierFactory.nextID(), partialBookingDTO.getCustomerDTO().asCustomer(),
					room , partialBookingDTO.getBookingDates().asBusinessPeriod(), BookingStatus.OPEN);

			bookingRepository.save(booking);

			return bookingAssembler.toResource(booking);
		}
		else{
			throw new RoomNotAvailableException(partialBookingDTO.getRoomDTO().get_id());
		}
	}


	public String cancelBooking(String id) {

		Booking booking = bookingRepository.findOne(id);

		if(booking != null){

			long numDays = ChronoUnit.DAYS.between(LocalDate.now() , booking.getBookingDates().getStartDate());

			if(numDays >= 10){
				// free cancellation
				booking.setBookingStatus(BookingStatus.CANCELLED);
				bookingRepository.save(booking);
				return "Booking has been cancelled";
			}
			else{
				//customerRepository.save(id);

				return "Booking cancelled and 50% fee deducted.";
			}

		}

		return "Booking cannot be found";
	}

	public BookingDTO closeBookingOrder(String id) {
		Booking booking = bookingRepository.findOne(id);
		booking.setBookingStatus(BookingStatus.CLOSED);
		bookingRepository.save(booking);
		return bookingAssembler.toResource(booking);

	}

}
