package com.example.demo.customer.application.dto;

import com.example.demo.customer.domain.model.Customer;
import com.example.demo.customer.domain.model.CustomerType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class CustomerDTO {
	String _id;
	String name;
	String address;
	String email;
	CustomerType customerType;

	public Customer asCustomer(){
		return Customer.of(_id, name, address, email, customerType);
	}
}
