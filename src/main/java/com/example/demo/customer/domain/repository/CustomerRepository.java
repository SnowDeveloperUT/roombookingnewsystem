package com.example.demo.customer.domain.repository;

import com.example.demo.customer.domain.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/21/2017.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, String> {}
