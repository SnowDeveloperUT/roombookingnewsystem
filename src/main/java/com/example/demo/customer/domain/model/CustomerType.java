package com.example.demo.customer.domain.model;

/**
 * Created by snowwhite on 6/21/2017.
 */
public enum CustomerType {
	USER, AGENCY
}
