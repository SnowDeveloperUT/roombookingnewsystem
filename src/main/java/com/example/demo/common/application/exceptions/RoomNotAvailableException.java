package com.example.demo.common.application.exceptions;

/**
 * Created by snowwhite on 6/21/2017.
 */
public class RoomNotAvailableException extends Exception {

	public RoomNotAvailableException(String id) {
		super(String.format("Room not available! (Room id: %s)", id));
	}

}
