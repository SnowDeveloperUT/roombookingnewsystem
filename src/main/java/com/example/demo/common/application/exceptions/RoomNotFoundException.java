package com.example.demo.common.application.exceptions;

/**
 * Created by snowwhite on 6/21/2017.
 */
public class RoomNotFoundException extends Exception {

	public RoomNotFoundException(String id) {
		super(String.format("Room not available! (Room id: %s)", id));
	}

}
