package com.example.demo.common.application.exceptions;

/**
 * Created by snowwhite on 6/21/2017.
 */
public class CustomerNotFoundException extends Exception {
	public CustomerNotFoundException(String id) {
		super(String.format("Customer not found! (Customer id: %s)", id));
	}
}
