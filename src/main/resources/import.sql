insert into room (id, price_per_night, room_type) values (1, 100, 'SINGLE')
insert into room (id, price_per_night, room_type) values (2, 150, 'DOUBLE')
insert into room (id, price_per_night, room_type) values (3, 100, 'SINGLE')
insert into room (id, price_per_night, room_type) values (4, 150, 'TWIN')
insert into room (id, price_per_night, room_type) values (5, 200, 'SINGLE')
insert into room (id, price_per_night, room_type) values (6, 300, 'TWIN')
insert into room (id, price_per_night, room_type) values (7, 600, 'STUDIO')


insert into customer(id, address, customer_type, email, name) values (1, 'Tartu', 'USER', 'abc@gmail.com', 'John')
insert into customer(id, address, customer_type, email, name) values (2, 'Tartu', 'USER', 'abc0@gmail.com', 'Johnathan')
insert into customer(id, address, customer_type, email, name) values (3, 'Tallinn', 'AGENCY', 'abc1@gmail.com', 'Johnpa')
insert into customer(id, address, customer_type, email, name) values (4, 'Tartu', 'USER', 'abc2@gmail.com', 'Jane')


insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (1, '2017-01-01', '2017-02-04', 'CREATED', 1, 1)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (2, '2017-01-03', '2017-02-02', 'PENDING', 2, 2)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (3, '2017-01-05', '2017-02-03', 'CREATED', 3, 3)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (4, '2017-01-06', '2017-02-21', 'REJECTED', 4, 4)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (5, '2017-01-02', '2017-02-20', 'CREATED', 1, 5)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (6, '2017-01-01', '2017-02-12', 'CLOSED', 2, 6)
insert into booking (id, end_date, start_date, booking_status, customer_id, room_id) values (7, '2017-01-04', '2017-02-22', 'CREATED', 3, 7)